from django.http import HttpResponse
from hellos.models import Hello

# Create your views here.
def index(request):
    hellos_list = Hello.objects.all()
    output = '<br>'.join(h.text for h in hellos_list)
    return HttpResponse(output)
