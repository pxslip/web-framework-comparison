# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Hello.create([{text: 'Hello World'}])
Hello.create([{text: 'Hello USA'}])
Hello.create([{text: 'Hello Washington, DC'}])
Hello.create([{text: 'Hello GWU'}])
