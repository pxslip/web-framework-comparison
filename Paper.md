# A Comparison of Web Application Frameworks #
##### Will Kruse - CSCI 6221, Fall 2014 ####
##### Source code available at [https://bitbucket.org/wskruse/web-framework-comparison](https://bitbucket.org/wskruse/web-framework-comparison) #####


## <a name="abstract"></a>1. Abstract ##

One of the most popular tools for building web based applications is the Web Application Framework. These frameworks generally provide a set of tools to simplify a developer’s job. These tools often, but not always, include routers, database abstractions, html templating libraries and a plugin architecture. However, each MVC framework approaches it’s task differently, some providing only a core set of libraries and relying on third parties to add functionality through a plugin architecture. Others provide all of the tools within the core, but are strongly opinionated about how the application should be built.

Beyond the differences in the way a framework functions, there are differences at the most fundamental level. This is the fact that frameworks cross programming languages, every language from Java to Python, PHP to Go have frameworks, often dozens or even hundreds.

This paper aims to analyze some of the most popular frameworks from several languages. The most important points that will be considered are ease of use, how opinionated the framework is, available libraries, developer support, plugin availability and, arguably most importantly, speed. The frameworks that will be tested are Laravel(PHP), Yii(PHP), Springs(Java), Struts(Java), Express(NodeJS), Martini(Go), Django(Python), Ruby on Rails(Ruby)

As each of these frameworks are significantly different, a baseline task is required for comparison. This task will be going from an untouched, linux based server to a simple application that retrieves data from a database and returns it to the user. Doing this task will show the ease of use, time for setup, and speed. A thorough reading of the developer documentation, and any available literature will provide information regarding the plugin availability, available libraries and developer support.

## Outline ##

1. [Abstract](#abstract)
2. [Introduction and Motivation](#intro)
3. [Frameworks](#frameworks)
    1. [PHP](#php)
        1. [Lanuage Notes](#php-lang-notes)
        2. [Laravel](#laravel)
            1. [Requirements](#laravel-req)
            2. [Installation](#laravel-install)
            3. [Functionality](#laravel-func)
            4. [Thoughts](#laravel-thoughts)
        3. [Yii](#yii)
            1. [Requirements](#yii-req)
            2. [Installation](#yii-install)
            3. [Functionality](#yii-func)
            4. [Thoughts](#yii-thoughts)
    2. [Rust](#rust)
        1. [Lanuage Notes](#rust-lang-notes)
        2. [Nickel](#nickel)
            1. [Requirements](#nickel-req)
            2. [Installation](#nickel-install)
            3. [Functionality](#nickel-func)
            4. [Thoughts](nickel-thoughts)
    3. [Ruby](#ruby)
        1. [Lanuage Notes](#ruby-lang-notes)
        2. [Ruby on Rails](#rails)
            1. [Requirements](#rails-req)
            2. [Installation](#rails-install)
            3. [Functionality](#rails-func)
            4. [Thoughts](#rails-thoughts)
    4. [Javascript](#javascript)
        1. [Lanuage Notes](#javascript-lang-notes)
        2. [Express](#express)
            1. [Requirements](#express-req)
            2. [Installation](#express-install)
            3. [Functionality](#express-func)
            4. [Thoughts](#express-thoughts)
    5. [Python](#python)
        1. [Lanuage Notes](#python-lang-notes)
        2. [Django](#django)
            1. [Requirements](#django-req)
            2. [Installation](#django-install)
            3. [Functionality](#django-func)
            4. [Thoughts](#django-thoughts)
    6. [Go](#go)
        1. [Lanuage Notes](#go-lang-notes)
        2. [Martini](#martini)
            1. [Requirements](#martini-req)
            2. [Installation](#martini-install)
            3. [Functionality](#martini-func)
            4. [Thoughts](#martini-thoughts)
4. [Analysis](#analysis)
5. [Conclusion](#conclusion)
6. [Appendix A](#appendix-a)
7. [Bibliography](#bibliography)


## <a name="intro"></a>Introduction and Motivation ##

Development of web applications is a major aspect of the modern programming industry. To simplify the work required to prop up a web application developers have built frameworks that allow for easy bootstrapping of a web application. Each of these frameworks has a different set of features, requirements, and purposes. By comparing some of the more popular frameworks one can determine what are common features, and what situations are best suited to a given framework. Herein seven frameworks are compared for several criteria, ease of installation, ease of use, features, opinionation, and speed.

## <a name="frameworks"></a>3. Frameworks ##

### <a name="php"></a>3.1. PHP ###

PHP was originally created in 1994 as a set of Common Gateway Interface tools by Rasmus Lerdorf. He created these tools to track visitors to his websites. As this set of tools grew in popularity Lerdorf rebuilt them to provide additional functionality. In 1995 Lerdorf open sourced the code for his "Personal Home Page Tools." PHP was rebuilt and renamed several times over the next few years, being known as both Forms Interpreter (FI) and PHP/FI. It wasn't until 1998 when Andi Gutmans and Zeev Suraski, students in Tel Aviv, that PHP began the trajectory that leads to today. Gutmans and Suraski set about rebuilding the PHP parser to better support their goal of building an eCommerce site. Working with Lerdorf, Gutmans and Suraski began building the language that would become known as PHP (PHP: Hypertext Preprocessor). PHP has had two major version releases since 1998. Version 4 was released in 2000 and built upon the modularity of PHP 3 as well as improving performance. Version 5, the current version, was released in 2004 and pushed the Object Oriented portions of the language to improve reusability as well as adding many new concepts to the language such as namespacing.[3]

### <a name="php-lang-notes"></a>3.1.1. PHP Language Notes ###

PHP is now, though was not always, considered an Object Oriented, Imperative programming language. To clarify the previous statement, many of the Object Oriented features of PHP were added to the language in the last two releases. PHP's syntax was originally designed to reflect the C programming language, and still retains many of those characteristics. PHP has also started to introduce many functional language features, including lambda functions and closures.

According to Web Technology Surveys PHP is one of the most dominant programming languages in use on the internet. Specifically, their studies indicate that "PHP is used by 82.0% of all the websites whose server-side programming language we know."[1] This marketshare means that there is a glut of available frameworks to test. According to a survey by sitepoint.com the most common frameworks are currently Laravel with 25.85% market share and Phalcon with 16.73% market share. Synfony2, CodeIgniter and Yii all have market shares in the 7-10% range.[2] The two PHP frameworks used in this comparisoon were chosen for their philosophical differences. Namely the difference in required formatting. Laravel allows the developer many options for how to configure the application, whereas Yii is highly opinionated and enforces the principles of MVC.

Despite it's popularity PHP has strong opposition in the developer community, a brief google search turns up these conversations regarding this - http://www.reddit.com/r/PHP/comments/1fy71s/why_do_so_many_developers_hate_php/, and https://news.ycombinator.com/item?id=7123012. In addition an entire website is devoted to why a developer does not like PHP (forgive the bad website design...) - http://webonastick.com/php.html.

### <a name="laravel"></a>3.1.2. Laravel ###

After the release of PHP 5.3 PHP developers began to look for a framework that incorporated the new functionality. Namely there was interest in namespacing and using closures in frameworks. However, there was no framework available at the time that used the new functionality. Codeigniter, the most popular framework at the time, was well loved by the community for its functionality and ease of use, but the code was starting to fall behind. Inspired by this lack Taylor Otwell released a new PHP framework, Laravel. Over the next three years Laravel has had 4 major releases, unimaginatively named Laravel1-Laravel4. While this continuous release of new versions indicated strong support from both the creator and the community, it made some developers nervous as each new release broke old code. To combat this concern Laravel has established a minor version release every six months, and continuous patching.[4]

Laravel is a full MVC framework. It's modeling is performed, by default, by the Eloquent Object-Relational Mapping(ORM), a custom tool developed for use with Laravel. As of Laravel2 Laravel supported class based controllers, however Laravel also allows for closure based routing, allowing the developer to choose which idiom they prefer. The templating enging that ships with Laravel by default is called Blade. Blade is also a custom tool developed for Laravel.[4]

### <a name="laravel-req"></a>3.1.2.1. Requirements ###

Laravel requires PHP 5.4 as well as the PHP MCrypt extension. PHP 5.4 provides the support required for closures and namespacing. MCrypt is used by Laravels integrated authentication mechanism.[5]

### <a name="laravel-install"></a>3.1.2.2. Installation ###

Laravel uses Composer as its installation tool.[5] Composer is a PHP dependency management tool, much like brew, apt, yum or any of the other installation tools, but designed specifically for PHP projects.[6] To install laravel one has three options. The first is to install and use the Laravel installer via the following commands.

```
composer global require "laravel/installer=~1.1"
~/.composer/vendor/bin/laravel new <app-name>
```

Another option is to use composer's built in create-project command.

```
composer create-project laravel/laravel --prefer-dist
```

The final option is to manually download, extract, and install Laravel's files. One must then run `composer install` to install the required dependencies.[5]

### <a name="laravel-func"></a>3.1.2.3. Functionality ###

As mentioned earlier Laravel's routing allows for either closure based or class based routing. An example of closure based routing is as follows.

```php
Route::get('hello', function() {
    return "Hello World!";
});
```
An example of class based routing is as follows.

```php
//routes.php
Route::get('/', 'HomeController@showWelcome');

//HomeController.php
class HomeController extends BaseController {

    public function showWelcome()
    {
        $hellos = Hello::all();
        return View::make('hello')->with('hellos', $hellos);
    }

}

```

Having these two distinct methods for routing to a function allows the developer to choose methods that work well for there taste, or the current project.[7]

Laravel's built in ORM, Eloquent, is extremely simple to use. The following example is a complete implementation of a model.

```php
class Hello extends Eloquent {}
```

While this example is simple to implement, it does rely on certain assumptions. Primarily it requires that you have a table named hellos configured in your database.[7]

The final part of MVC in Laravel is the templating engine, Blade. An example layout, and view file are included here to highlight the syntax.[7]

```html
<!--layout.blade.php-->
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Laravel PHP Framework</title>
</head>
<body>
  <div class="content">
    @yield('content')
  </div>
</body>
</html>

<!--hello.blade.php-->
@extends('layout')

@section('content')
    @foreach($hellos as $hello)
    <p>{{$hello->text}}</p>
    @endforeach
@stop
```
Obviously Blade includes the ability to extend a layout file through the `@yield` and `@extends` keywords. It also allows for some simple logic using the `@if`, `@foreach`, `@for`, and `@forelse` keywords.[8]

Laravel provides many other features such as built-in authentication, CSRF protection, payment mechanisms, caching, event listeners, and many more.[7]

### <a name="laravel-thoughts"></a>3.1.2.4. Thoughts ###

Laravel provided one of the easiest set-ups of all of the frameworks tested. Installing composer was easy using the homebrew[10] package manager for Mac OS, and composer made the task of setting up Laravel almost too easy. Launching the server was also straightforward as PHP >= 5.4 provides a built in web-server for testing and development, so the grunt task simply uses grunt-php to start a php server in `laravel/public`.[9] Wiring up the database was as easy as changing the path in `laravel/app/config/database.php` to the test database's path.

### <a name="yii"></a>3.1.4. Yii ###

Yii was developed in 2008 by one Qiang Xue. He chose to build Yii to meet, what he considered to be an unmet, need for "an extremely fast, secure and professional framework."[11] Yii claims to reference several other projects in its design, including Symfony, Ruby on Rails, Joomla and Prado.

### <a name="yii-req"></a>3.1.4.1. Requirements ###

Yii 2.0 requires that it run on a server running PHP 5.1.0 or above to function.[12] In addition, during installation Yii requires the Composer Asset plugin to be installed.

### <a name="yii-install"></a>3.1.4.2. Installation ###

As with Laravel, Yii uses composer to perform installation. The installation commands are as follows, and were found to work without any additional configuration.

```
composer global require "fxp/composer-asset-plugin:1.0.0-beta3"
composer create-project yiisoft/yii2-app-basic basic 2.0.0
```
The first command includes the required Composer Asset plugin, the second creates a demo project for Yii and installs all of the coposer dependencies.[12]

### <a name="yii-func"></a>3.1.4.3. Functionality ###

Yii is strongly a MVC framework, enforcing this through "convention over configuration."[11] This does mean that the developer has less leeway with the framework. The first example of this is the controller. Yii only allows class based controllers, and automatically configures routes based on the Controller class, and function name. An example controller that routes to /hello/index (or /hello as index is the default action).

```php
class HelloController extends Controller {
    public function actionIndex() {
        return $this->render('index');
    }
}
```

Yii models are almost indistinguishable from the Eloquent ORM in Laravel, using the class name to locate the table and auto-populating the properties. An example model that maps to a table named hellos follows.
```php
class Hello extends ActiveRecord {

}

```

The final aspect of MVC, the view, is unusual in Yii as Yii has no templating engine, simply using the built in ability for PHP to be interleaved with HTML content. An example Yii view follows.
```php
<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">
      <?php foreach($hellos as $hello): ?>
        <p><?php print $hello->text; ?></p>
    </div>
</div>

```

### <a name="yii-thoughts"></a>3.1.4.4 Thoughts ###

Yii's major distinguishing characteristic is it's preference for "convention over configuration."[11] This preference forces the developer to use only the method that Yii permits, with a sort of Yii's way or the highway mindset. The upside to this is that all Yii developers know what to expect, and how code is configured, regardless of the purpose of the application.

Yii has a large and active set of contributors, with almost 400 contributors on the Yii2 github repository having produced over 10,000 commits. Yii also supports extensions, and has a collection of almost 2000 listed on it's site.

### <a name="rust"></a>3.2 Rust ###

Rust is a systems programming language designed to supplement the use of C and C++ in programming. Graydon Hoare was the first developer of Rust, but after it's initial development Mozilla built a team around Hoare to complete the work. Mozilla chose to fund the development of Rust to use it as a replacement for C++ in the browser. Rust is still considered an alpha project, but has attracted developer attention for its ease of use, safety, and simplicity in comparison to C.[13]

### <a name="rust-lang-notes"></a>3.2.2 Language Notes ###

Rust's syntax is designed to be familiar to anyone who has previously used C/C++/Java. However there are some interesting features that make Rust different. The first is that all variables are considered immutable by default, the developer must explicitly declare a variable to be mutable.[14] Additionally Rust has full support for some functional programming conveniences like closures. Rust also has type inference, generics, traits and several other modern lanuage features.

### <a name="nickel"></a>3.2.3 Nickel ###

The history of Nickel is almost non-existent. The first commit to the github repository was by Christoph Burgdorf on June 3, 2014. In addition the users Ryman(https://github.com/Ryman) and Simon Persson(https://github.com/SimonPersson) are heavy contributors to the Nickel project.

### <a name="nickel-req"></a>3.2.3.1 Requirements ###

Nickel has no dependendencies outside of having the Rust language installed on server or development machine.

### <a name="nickel-install"></a>3.2.3.2 Installation ###

Once Rust and Rust's dependency manager Cargo are installed there are two steps to install Nickel. The first is to use Cargo to generate the project via

```
cargo new <project-name> --bin
```
Passing the `--bin` option informs cargo that this project is not a library project, but runnable code. Once the project is created one needs to edit the Cargo.toml file that defines the project dependencies, adding the following.

```
[dependencies.nickel]

git = "https://github.com/nickel-org/nickel.rs.git"
```

Once that is done running the following command will install nickel and all of its dependencies and start the application.

```
cargo run
```
[15]

### <a name="nickel-func"></a>3.2.3.3 Functionality ###

Nickel is an extraordinarily bare bones framework. It's functionality is limited to handling requests and routing those requests to a function based on the uri. The basic Nickel server is extraordinarily short, less than 10 lines:

```
extern crate nickel;

use std::io::net::ip::Ipv4Addr;
use nickel::{ Nickel, Request, Response, HttpRouter };

fn main() {
    let mut server = Nickel::new();

    fn a_handler (_request: &Request, response: &mut Response) {
        response.send("hello world");
    }

    server.get("/hello", a_handler);
    server.listen(Ipv4Addr(127, 0, 0, 1), 6767);
}
```
Nickel provides no built in ORM, or templating engine. The upside of this simplicity is that the developer can choose tools from the, admittedly small, list of libraries for the rust language. For example looking at http://rust-ci.org/projects/#template%20engine one can select a templating engine for their framework.

### <a name="nickel-thoughts"></a>3.2.3.4 Thoughts ###

While Nickel is an extraordinarily young framework it appears to be gaining attention quickly. The one issue during usage that was encountered was that the development pace of Rust is so fast that occasionally changes to the language will break the framework. Specifically, the [groupable-rs](https://github.com/nickel-org/groupable-rs) library failed to compile due to a breaking change in the language.

### <a name="ruby"></a>3.3 Ruby ###

Ruby's first public release was in 1995, a project by Yukihiro Matsumoto. The reason Ruby was developed was to address certain flaws in extant scripting languages. The first truly stable relesae of Ruby was in 1998 with version 1.2. Ruby's real popularity began with the release of Ruby on Rails in 2005. With the release of Rails many web developers flocked to Ruby as a straightforward framework. OVer the course of its almost 20 year lifetime Ruby has garnered a large group of ardent supporters.[16]

### <a name="ruby-lang-notes"></a>3.3.1 Language Notes ###

Ruby's design was focused on making it a fully object-oriented programming language. Ruby has several notable features that make it unique. The first is that all instance variables, unless otherwise defined, are considered private. Ruby also includes a metaprogramming technique to quickly define accesor and mutator methods on those variables. Another interesting feature, though not unique to Ruby, is duck typing. This allows a developer to emulate inheritance, without specifically inheriting from another class. One final note regarding Ruby is that it is a dynamically typed language.[17]

### <a name="rails"></a>3.3.2 Ruby on Rails ###

Ruby on Rails was a project that came out of the development of the Basecamp project management tool. It's developer, David Heinemeier Hansson, was building this application and in the process created a Model-View-Controller framework. The Rails framework was first officially released in 2004. Rails emphasizes "convention over configuration" in an attempt to improve developer productivity. This allows a programmer to easily build out a web application with little work. This also means that developers will find it easiest to follow the conventions laid out by Rails, and deviating will introduce extra work.[18]

### <a name="rails-req"></a>3.3.2.1 Requirements ###

Rails requires Ruby version 1.9.3 or newer. Though Rails recommends version 2.1 or newer.[19]

### <a name="rails-install"></a>3.3.2.2 Installation ###

Once an appropriate version of of Ruby is installed Rails can be installed using RubyGems as follows.

```
gem install rails
```

Once Rails is installed creating a new project uses the following syntax.

```
rails new <path/to/app>
```

This command will create the skeleton of a ruby application.[20]

### <a name="rails-func"></a>3.3.2.3 Functionality ###

As Rails is a strongly MVC framework there are at minimum two elements to create a page that can be accessed. The first is a controller method to handle the backend logic. The second is a view file to handle the layout. Finally, if the view interfaces with a database it requires a model file for ORM. The simple controller file that pulls data from the sqlite database follows.

```ruby
class HomeController < ApplicationController
    def index
        @hellos = Hello.all
    end
end
```

The view file that renders the content appears as below. One thing that does not appear in the following code is the use of Rails' helper methods. An example is the `link_to` method which generates a well formatted link to the specified route.

```
<h1>Hellos!</h1>
<% @hellos.each do |hello| %>
<h2><%= hello.text %></h2>
<% end %>
```

Finally, the model that defines a `hello` is defined as follows.

```
class Hello < ActiveRecord::Base
end
```

Another feature worth noting is Rails' command line interface. This rails cli provides an utility that can be used for several things in addition to creating a new project. The first is the ability to generate aspects of an exisiting project, such as creating a new controller with its routes and views, creating models or creating helper classes. If wanted to create a new HelloController one could use the rails command as follows.

```
rails generate controller Hello action1 action2
```

Or one could use it to create a model, complete with a migrations file to modify the database, using the following command.

```
generate model hello text:text
```

This command creates a Hello model that includes a single field, the text field. The migration file generated will create a hellos table with that field, as well as the internally used id, created_at, and updated_at fields.[21]

### <a name="rails-thoughts"></a>3.3.2.4 Thoughts ###

Rails provides a strong application structure, that encourages good practices. This helps the novice developer to get started in this framework, while not providing the freedom, it pushes a developer to use good practices from the start. As with other frameworks that include ORM creation of models is exceedingly simple, and acessing the data contained in them is equally simple.

### <a name="javascript"></a>3.4 Javascript ###

Javascript arose out of the browser wars of the mid 90s. In an attempt to get a leg up on its competitors Netscape worked with Brendan Eich to develop a scripting language that could be used to create dynamic webpages. The first version of Javascript shipped in 1995 with Netscape 2. Javascript was standardized by Ecma International in 1997. At the same time that development of Javascript continued at Netscape, Microsoft sought to have similar functionality and began development of JScript, a Javascript dialect.[21] Javascript, or more correctly ECMAScript(ES) after the ECMA standard it is based on, is somewhat unusual due to the fact that there are several commonly used dialects. Specifically each browser engine can implement its own version of ES. Currently there are several engines in use, including Nitro, Rhino, SpiderMonkey, and V8.[22] The two concepts that truly drove the adoption of Javascript through the late 90s were Asynchronous Javascript and XML(AJAX) and Document Object Model(DOM) manipulation. The first allows a web page to asynchronously communicate with a server, preventing full page reloads. Originally intended to retrieve XML from the server, thus the name, AJAX has expanded to retrieve any data type from the server, from XML to JSON, Images and HTML. DOM manipulation allows the Javascript developer to change elements on a webpage on the fly, changing images, modifying text or implementing the dreaded blink tag.[23] Over the next few years development of the ES standard continued, though not without roadbumps. Attempts to improve on ES by adding full OO support, operator overloading and other features met with resistance and derailed ES version 4. The mid 2000s saw an explosion of Javascript libraries developed to better support the two major features of ES, some of the most popular are Dojo, Prototype, and jQuery. Many of these libraries still exist today.[24] One of the more interesting developments of the last few years is the release of NodeJS. Node is a server side runtime environment based on the V8 ES engine developed by Google, its first release was in 2009 by Joyen, Inc. This environment allows a programmer to use ES on both the client and server side.[27]

### <a name="javascript-lang-notes"></a>3.4.1 Language Notes ###

ES is an interpreted, dynamically typed, prototype based language. The key feature that makes ES unusual is the fact that it is prototype based. This means that there is no traditional inheritance as in most OO languages. Prototype based languages eschew class definitions, preferring a more concrete approach of attaching new behaviors that an object can access from its parent object. There are many complaints regarding ES that must be addressed. The first, and one that is shared with PHP and likely other dynamically typed languages, is the boolean operators. ES provides two types of equality operators. The first type, `==` and `!=`, allow for type coercion, thus `1 == "1"` will return true. While this can be useful in some circumstances a naive developer may cause difficult to discover bugs using this. The second issue worth noting is the `eval` function. This function will execute javascript from a string, the true danger in this function is when it is used with untested, or untrusted strings as this can throw errors, or act maliciously. Three of the worst aspects of ES are global variables, scoping, and lax semicolons. The first revolves around that fact that ES currently lacks any concept of modules, packages or similar concepts. This means that to access another set of functions or variables requires these globals. Additionally, globals are easy for the novice ES developer to create. Simply writing `var foo = "foo"` outside of a function or `foo = "foo"` anywhere in a program will attach the variable foo to the global `window` object. Scoping in ES is difficult as it does not use block scoping, thus what should be a temporary variable declared in a `for` loop is suddenly accessible to the rest of the function. The final issue worth mentioning is the fact that ES allows the developer to leave the semicolon off the end of a statement. Again this introduces issues for the naive or novice programmer.

### <a name="express"></a>3.4.2 ExpressJS ###

Express' history has very little information on it. It was originally released in June of 2009 by TJ Holowaychuk. After several years of development, the project was handed to the community for continued maintenance as TJ had lost interest.[28] Finally in July of 2014 sponsorship of Express was transferred to StrongLoop a company selling services based on the Express framework.[29]

### <a name="express-req"></a>3.4.2.1 Requirements ###

Express primarily requires two elements. The first is a working installation of Node, and the second is a functional version of NPM, node's package manager.

### <a name="express-install"></a> 3.4.2.2 Installation ###

Exress has an extremely simple installation process, however it does provide two approaches that should be considered. The first is to simply install express as a dependency and create the application files by hand. This is accomplished with the following.

```bash
mkdir <app-dir>
cd <app-dir>
npm init
npm install express --save
```

A more robust option is to use the express generator. The generator is installed with the following command.

```bash
npm install express-generator -g
```

Once installed one can use the express generator command line tool as follows.

```bash
mkdir <app-dir>
cd <app-dir>
express <app-name>
npm install
```

The express generator will create a skeleton application with much of the boilerplate code already generated.

### <a name="express-func"></a>3.4.2.3 Functionality ###

Express is not an MVC framework on its own. What it does provide is the basic set of functionality necessary to stand up a web application. That being said the number of available middleware packages available for express are extensive. With those one can turn express into a fully MVC framework.

Express' routing is primarily closure based, with many routes being defined immediately within a routes file, however it is recommended that the developer use Node's module system to build a set of route handlers and export them to the main application. An example, closure based, route is shown below.

```javascript
router.get('/', function(req, res) {
    db.all("SELECT text FROM hellos", function(err, rows) {
        console.log(rows);
        res.render('index', { rows: rows });
    });
});
```

In addition to its powerful routing capabilities, though those can have downsides as found out by Netflix[30], Express comes with a built in templating engine called Jade. Jade is intended to make creation of templates succinct. An example follows.

```jade
extends layout

block content
    h1= "Hellos!"
    each val in rows
        h2= val.text
```

### <a name="express-thoughts"></a>3.4.2.4 Thoughts ###

Express' strength is in its flexibility. With the breadth of middleware available, and the flexible coding style the developer is left with the freedom to build an app in whatever style they prefer. This benefit is also a danger as a naive developer may build an unsustainable application with little difficulty. Another benefit of Express is the wide adoption, there are many hosting platforms available that specialize in node applications and there are many tools aimed at those applications. For example [nodejitsu](https://www.nodejitsu.com/) provides hosting aimed at Node, and [grunt](http://gruntjs.com/), is built on top of node.

### <a name="python"></a> 3.5 Python ###

Python was first released to the public in the early 1990s. It was developed by Guido van Rossum to replace ABC. Throughout the early 1990s Python was developed by [Stichting Mathematisch Centrum](http://www.cwi.nl/) before Rossum moved to [Corporation for National Research Initiatives](http://www.cnri.reston.va.us/). 2001 was a major year for Python as it saw the creation of the [Python Software Foundation](https://www.python.org/psf/) an organization designed to own the intellectual property associated with Python. All releases after 2001 have been owned by PSF.[31]

### <a name="python-lang-notes"></a> 3.5.1 Language Notes ###

Python is both a dynamically and strongly typed language. The compiler maintains track of the type of values as they are assigned to variables, however variables are allows to contain any value. Additionally Python does not allow for coercion, so adding a string value to a numeric value will result in an error.[32] Python is an OO language, but does allow for scripting style programs.[33] Finally, python is a language that does not have a line terminator character, and relies heavily on indentation for correct execution.[34]

### <a name="django"></a> 3.5.2 Django ###

Django evolved out of a real world situation. Adrian Holovaty and Simon Willison, developers at *Lawrence Journal-World*, found it necessary to build code to quickly bootstrap web applications. Django was first released in 2005 after two years of incubation. Django is primarily a web application framework, but it does draw inspiration from its journalism roots. This is displayed through its admin site that provides CMS style content creation.[35]

### <a name="django-req"></a> 3.5.2.1 Requirements ###

Django requires Python 2.7, 3.2 or 3.3. While there are other options, installation of Django is done most easily using python's package manager pip.[36]

### <a name="django-install"></a> 3.5.2.2 Installation ###

The easiest way to install Django is using pip, to do so one runs the following.[36]

```bash
pip install Django
```

Once the global Django package is installed a new django project is created using the following commands.

```bash
mkdir <project-dir>
cd <project-dir>
django-admin.py startproject <project-name>
```

This command only creates a basic project, to actually develop a working web project one runs the following.

```bash
python manage.py startapp <app-name>
```

Django comes with a built in administration tool that requires a database to function. There is a built in migrations tool that will create the tables for this admin page. This is done using the commands below.

```bash
python manage.py migrate
```

### <a name="django-func"></a> 3.5.2.3 Functionality ###

Python is a fully MVC framework, providing an easy way to create models, view and controllers. Within an app created with the `manage.py startapp` command there are files for each of these aspects. One odd aspect of this is that the controllers are all located in the `views.py` file. Routing in the application is configured in one of the project files, specifically `urls.py` where a static string or pattern is matched to any method desired.

```python
urlpatterns = patterns('',
    url(r'^$', 'hellos.views.index', name='home'),
)
```

A controller method handles backend logic and can either return a static string, or use Django's built in render tool to render a template file.

```python
def index(request):
    hellos_list = Hello.objects.all()
    output = '<br>'.join(h.text for h in hellos_list)
    return HttpResponse(output)
```

The controller above uses Django's model tool, which maps a python data structure to a database table. An example model is below.

```python
class Hello(models.Model):
    text = models.TextField()
```

Python automatically adds several fields to any model, including a creation date, an update date, and a row id. The final noteworth feature of Django is its admin site. Once registered an application can be managed through this interface. This provides a CMS style web interface to create new content based on the application models.

### <a name="django-thoughts"></a> 3.5.2.4 Thoughts ###

Django is unique in providing a web based interface for creating content, this is a throw back to its origins as a publishing tool for newspapers. Django's approach to app structure is similar to many of the other frameworks in that it allows the developer to follow their own style.

### <a name="go"></a> 3.6 Go ###

Often compared to Rust, Go is a systems language aimed at the C/C++ developer. It originated with a team of three developers, Robert Griesemer, Rob Pike, and Ken Thompson. These three developers started the project during their tenure at Google, who soon embraced the language for its own use. Go was officially released in November of 2009, making it one of the younger languages in the comparison. The aim of Go is to produce a systems language that provided benefits not available in the current landscape. Some of those benefits include speed of compilation, dependency management, a modern typing system with type inference and a goal to easily support concurrency.[38]

### <a name="go-lang-notes"></a> 3.6.1 Language Notes ###

Go is a statically typed language, however it does support type inference through the `:=` operator. As with many systems languages Go does not support traditional OO features, though like C it does support structs to create an object-like system. Go does provide the ability for structs to inherit functionality from other structs providing a degree of inheritance.[39]

### <a name="martini"></a> 3.6.2 Martini ###

Martini has a poorly documented history, and is a relatively young framework in the list of frameworks included here. The first commit to the [martini repository](https://github.com/go-martini/martini) was in October of 2013 by Jeremy Saenz.[40]

### <a name="martini-req"></a> 3.6.2.1 Requirements ###

Martini simply requires the Go programming language, version 1.1 or greater, to be installed on the computer where it will be used.

### <a name="martini-install"></a> 3.6.2.2 Installation ###

Once Go is installed, Martini is added to the set of dependencies available using the following command.

```bash
go get github.com/go-martini/martini
```

Once installed martini can be used in a Go program by importing it as below.

```go
import "github.com/go-martini/martini"
```

### <a name="martini-func"></a> 3.6.2.3 Functionality ###

Martini primarily provides a routing interface that maps urls to handler functions, or closures. The following code maps the base url `"/"` to the closure.

```go
m.Get("/", func() string {
    //Access the database
    var buffer bytes.Buffer
    rows, _ := db.Query("SELECT text FROM hellos")
    for rows.Next() {
        var text string
        rows.Scan(&text)
        buffer.WriteString(text)
        buffer.WriteString("\n")
    }
    db.Close()
    return buffer.String()
    })
```

By default Martini does not provide anything beyond this simple routing functionality, and some functionality for modifiying requests and responses. However, [Martini-Contrib](https://github.com/martini-contrib) provides a list of available middleware designed to be used with Martini. This includes authentication tools, request-forgery prevention, and templating.

### <a name="martini-thoughts"></a> 3.6.2.4 Thoughts ###

Martini, like many of the frameworks included in this comparison, provides a basic set of tools for handling requests ot a web application. Additional desired functionality must be included through the use of middleware. The largest barrier to entry is in fact Go itself, while preventing many naive errors, the syntax is more difficult to easily pick up and get started with.

## <a name="comparison"></a>4. Comparison of Frameworks ##

Comparing frameworks is a difficult proposition, this is due to the many factors involved in choosing a framework. These factors can include ease of installation, ease of use, learning curve, available features, and speed. While it is perhaps naive to compare a set of frameworks with such distinct features it does provide some information that a developer can use to choose a framework.

The first factor worth considering is ease of installation. Most of the frameworks chosen for this comparison have installation scripts that make creation of an application project extremely simple. Django, Express, Laravel, Rails, and Yii all include full project templates that allow the developer to get started with almost no extra effor. Martini and Nickel are more difficult, likely due to the yourh of the frameworks. Neither Martini nor Nickel have a project template, instead leaving the developer to choose how to organize the project with little to no input from the tool.

Ease of use is an important factor for the novice developer. Several of the applications include either command line or web based tools to help build content, or create application files automatically. For example Yii includes a web based tool that allows for creation of template files automatically. Alternatively Django includes a web based administration site that does not allow for creation of files, but does allow for adding database entries based on existing models. Beyond tools for creation of files or data, one must consider the difficulty, or lack thereof, of editing existing code. Yii has one of the simplest routing mechanisms available, automatically creating routes based on the controller names and functions. A final consideration is database interactions. Django, Laravel, Rails, and Yii all include extremely easy to use ORMs, often only requiring the creation of a correctly named class file.

Each framework has a level of opinionation that can influence how easy it is to pick up a framework. Rails, Django, and Yii are more opinionated, expecting files in certain places for best use. Laravel, and Express are less opinionated, allowing the developer more room for chossing how the project is laid out. Finally, Martini and Nickel have no opinionation whatsoever. There are both upsides and downsides to the more opinionated frameworks, namely giving novice developers a chance to create unsustainable applications.

Availability of features is another difficult characteristic to analyze this is because a framework may have features incorporated, but all of the frameworks compared here allow for incorporation of additional plugins or middleware. For example Express includes no ORM, or database tools, but does allow for easy use of available middleware, like the Mongoose ORM for the Mongo database. However, it is worth looking at the framework as it is provided to the developer without these third-party tools. With that condition the more established frameworks have an advantage, as Django, Laravel, Rails, and Yii all include all of the necessary tools to have a framework from database, to html templating

The final characteristic, speed, is likely one of the most important. To test the speed of these frameworks a [PhantomJS](http://phantomjs.org/) script was written to load each page repeatedly and record the time it takes for the page to load. While effort was made to make the basic page loaded as simple as possible, there are some additional resources that are included in some of these frameworks that may increase the load time unfairly. Additionally, the development servers used to load the page will introduce additional error that is difficult to account for. With those caveats mentioned Django was found to have the fastest load time, with an average of 3.74ms over 50 page loads.

## <a name="conclusion"></a>5. Conclusion ##

Within this paper is a comparison of several popular frameworks that are in use currently. They were compared on several key features, including ease of installation, ease of use, opinionation, features and speed. No framework arose as a one-size-fits all best framework. However, several key features appeared. The first is the how friendly a framework is to developers new to the field of web development. While no framework arose as the hands-down best several provide utility to the novice developer. Laravel, Yii, Rails, and Django all include simple to understand ORM and templating systems that allow a developer to get off the ground quickly. Yii, Rails, and Django all include project formats that help a novice developer to maintain best practices. The second feature worth noting is speed. Unexpectedly Django was the fastest of the frameworks, however that may be due to both the lack of additional content being loaded, and the development sever included with Python. Both Express and Martini had similar load times indicating that in situations where an experienced developer is looking for the fastest framework these three frameworks should be considered.

Comparing frameworks that are so distinct was, perhaps, a naive goal. However, it does provide information that may be invaluable to both new developers interested in starting with web development, and experienced developers interested in the available options.

### <a name="appendix-a"></a> 6. Comparison Table ###

| Frameworks 	| Installation                                                                                                                                                  	| Ease of Use                                                                                                                                                                       	| Opinionated                                                                                                            	| Features                                                                                                                                                      	| Speed                                                	|
|------------	|---------------------------------------------------------------------------------------------------------------------------------------------------------------	|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|------------------------------------------------------------------------------------------------------------------------	|---------------------------------------------------------------------------------------------------------------------------------------------------------------	|------------------------------------------------------	|
| Django     	| Simple, use `pip` to install Django, then `django-admin.py` to create a project.                                                                              	| Django includes a pre-configured app, the developer simply has to fill in the blanks to have a simple app up and running. Django also includes an admin interface for simplicity. 	| Django allows for the developer to choose how the app is laid out, but does have a preferred project configuration     	| Django includes a command line utility to create new applications within a project.                                                                           	| Load time over 50 loads: 3.74ms                      	|
| Express    	| Simple, once Node is installed use `npm` to install the `express-generator`.                                                                                  	| Express is easy to use, allowing for quick creation of route->function mapping.                                                                                                   	| Express allows for any application configuration that the developer chooses.                                           	| Express is feature-light, relying on a large pool of third-party middleware.                                                                                  	| Load time over 50 page loads: 8.74ms                 	|
| Laravel    	| Simple, uses `composer` to install a full project.                                                                                                            	| Similar to Express Laravel allows for easy mapping of routes to functions.                                                                                                        	| Laravel has a preferred configuration for a project, but does allow the developer to break from that if desired.       	| Laravel is a fully-featured framework, including an ORM and templating engine.                                                                                	| Load time over 50 page loads: 36.18ms                	|
| Martini    	| Simple, use the built in go dependency management tool to install martini, then import it into any go file.                                                   	| Martini is simple to configure, once one understands some of the quirks of the Go language.                                                                                       	| Martini has no opinion regarding project configuration, allowing the developer to choose any method they like.         	| Martini is also feature-light, requiring the developer to choose from available tools to supplement the core functionality.                                   	| Load time over 50 page loads: 6.46ms                 	|
| Nickel     	| Moderate, requires installation of the rust compiler, which can be complex. Additionally speed of development introduces breaking changes on a regular basis. 	| Similar to Martini, Nickel is simple to use, but requires a thorough understanding of the Rust language.                                                                          	| Nickel has no required configuration, allowing the developer to use as many or as few files to create the application. 	| Nickel has few built-in features, but does have a small pool of available middleware. The availability of these middleware is small due to the youth of Rust. 	| Due to broken middleware, no load time is available. 	|
| Rails      	| Simple, install rails with `gem` and then create a new application in one line.                                                                               	| Rails is relatively simple to use, requiring little work to have a basic application.                                                                                             	| Rails is more opinionated than some frameworks, requiring the developer to follow at least some of its configurations. 	| Rails includes a full ORM, and templating engine. In addition the rails cli allows for quick creation of new aspects of the application.                      	| Load time over 50 page loads: 42.6ms                 	|
| Yii        	| Simple, uses `composer` to create a full-working demo application.                                                                                            	| Requires some work to understand how the application is laid out, but simple once that is understood.                                                                             	| Yii is likely the most opinionated framework, requiring a lot of work to break out of the default configuration.       	| Yii includes an ORM, but relies on PHPs capability to be inserted into html files for templating.                                                             	|                                                      	|

## <a name="bibliography"></a>7. Bibliography ##
(http://www.ieee.org/documents/ieeecitationref.pdf)

1. http://w3techs.com/technologies/details/pl-php/all/all
2. http://www.sitepoint.com/best-php-frameworks-2014/
3. N.A. "History of php." Php.net, Web. November 14, 2014. <http://php.net/manual/en/history.php.php>.
4. Surguy, Maksim. "History of Laravel PHP framework, Eloquence emerging." Maxoffsky, Web. November 15, 2014. <http://maxoffsky.com/code-blog/history-of-laravel-php-framework-eloquence-emerging/>
5. http://laravel.com/docs/4.2/installation
6. https://getcomposer.org/
7. http://laravel.com/docs/4.2/quick
8. http://laravel.com/docs/4.2/templates#blade-templating
9. http://php.net/manual/en/features.commandline.webserver.php
10. http://brew.sh/
11. http://www.yiiframework.com/about/
12. http://www.yiiframework.com/download/
13. http://www.infoq.com/news/2012/08/Interview-Rust
14. http://opensource.com/life/14/6/mozillas-rust-programming-language-critical-stage
15. http://nickel.rs/getting-started.html
16. http://www.sitepoint.com/history-ruby/
17. <http://en.wikipedia.org/wiki/Ruby_(programming_language)#Features>
18. http://code.tutsplus.com/articles/ruby-on-rails-study-guide-the-history-of-rails--net-29439
19. http://rubyonrails.org/download/
20. http://guides.rubyonrails.org/getting_started.html
20. http://guides.rubyonrails.org/command_line.html
21. http://dailyjs.com/2010/05/24/history-of-javascript-1/
22. http://en.wikipedia.org/wiki/JavaScript_engine
23. http://dailyjs.com/2010/05/31/history-of-javascript-2/
24. http://dailyjs.com/2010/06/14/history-of-javascript/
25. http://www.lirmm.fr/~dony/postscript/proto-oopsla92.pdf
26. http://archive.oreilly.com/pub/a/javascript/excerpts/javascript-good-parts/bad-parts.html
27. http://nodejs.org/changelog.html
28. http://gilesbowkett.blogspot.com/2014/07/the-bizarre-bazaar-who-owns-expressjs.html
29. http://strongloop.com/strongblog/tj-holowaychuk-sponsorship-of-express/
30. http://techblog.netflix.com/2014/11/nodejs-in-flames.html
31. https://docs.python.org/2/license.html
32. https://wiki.python.org/moin/Why%20is%20Python%20a%20dynamic%20language%20and%20also%20a%20strongly%20typed%20language
33. http://www.tutorialspoint.com/python/python_overview.htm
34. http://www.tutorialspoint.com/python/python_basic_syntax.htm
35. http://www.djangobook.com/en/2.0/chapter01.html#django-s-history
36. https://docs.djangoproject.com/en/1.7/topics/install/
37. https://docs.djangoproject.com/en/1.7/intro/tutorial01/
38. https://golang.org/doc/faq#history
39. http://spf13.com/post/is-go-object-oriented/
40. https://github.com/go-martini/martini/commits/master?page=14
