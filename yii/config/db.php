<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'sqlite:db-yii.sqlite',
    'charset' => 'utf8',
];
