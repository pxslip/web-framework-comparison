# Notes on the Django framework #

- Installation instructions https://docs.djangoproject.com/en/1.7/intro/install/
- Easy installation, requires local python
  - `pip install django` installs global django tools and packages
  - `django-admin startproject <project-name> <dir/>` creates a barebones project
- These steps only create the basic setup, to add an actual app run `python manage.py startapp <app-name>` in the project directory
- To test use start the dev server with `python manage.py runserver [port]` port is optional and defaults to 8000
- The built in manage.py api allows for creation of new subapps, as well as db migrations
