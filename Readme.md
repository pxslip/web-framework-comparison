# A comparison of Web Frameworks #

This directory contains several demo applications that all reference a local sqlite database

## Goals ##

This project aims to compare, perhaps unfairly, several of the most popular web frameworks available

This list currently includes

- Django - a Python based framework
- Ruby on Rails - the ubiquitous Ruby based framework
- Express - the most common framework for use with Node.js
- Martini - one of the most commonly used frameworks for the young Go language
- Laravel - a popular PHP framework
- Yii - another PHP framework in wide use
- Nickel - a Rust based framework

## Execution ##

This project uses [Grunt](http://gruntjs.com/) to run each task. A task has been added for each framework, and can be run from the command line using `grunt <task>` where task can be any of laravel, yii, django, rails, express, martini, nickel.