var express = require('express');
var router = express.Router();
var fs = require("fs");
var file = "express/db-express.sqlite";
var exists = fs.existsSync(file);
if(!exists) {
    fs.openSync(file, "w");
}
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

/* GET home page. */
router.get('/', function(req, res) {
    db.all("SELECT text FROM hellos", function(err, rows) {
        console.log(rows);
        res.render('index', { rows: rows });
    });
});

module.exports = router;
