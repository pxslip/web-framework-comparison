/*global module:false*/
module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  // Project configuration.
  grunt.initConfig({
    // Task configuration.
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        unused: true,
        boss: true,
        eqnull: true,
        globals: {
          jQuery: true
        }
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib_test: {
        src: ['lib/**/*.js', 'test/**/*.js']
      }
    },
    nodeunit: {
      files: ['test/**/*_test.js']
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib_test: {
        files: '<%= jshint.lib_test.src %>',
        tasks: ['jshint:lib_test', 'nodeunit']
    },
        paper: {
            files: ['Paper.md'],
            tasks: ['shell:pandoc']
        }
    },
    shell: {
      martini: {
        command: 'go run ./martini/server.go'
      },
      express: {
        command: [
          'DEBUG=express',
          'express/bin/www'
        ].join('&&')
      },
      rails: {
        command: [
          'cd ./rails',
          'rails server'
        ].join('&&')
      },
      django: {
        command: 'python django/manage.py runserver'
      },
      nickel: {
        command: [
          'cd ./nickel',
          'cargo run'
        ].join('&&')
      },
      pandoc: {
        command: 'pandoc Paper.md -f markdown -t docx -s -o Paper.docx'
      }
    },
    php: {
      laravel: {
        options: {
          port: 8000,
          base: './laravel/public',
          keepalive: true,
          open: true,
        }
      },
      yii: {
        options: {
          port: 8001,
          base: './yii/web',
          keepalive: true,
          open: true,
        }
      }
    }
  });

  // Default task.
  grunt.registerTask('default', ['jshint', 'nodeunit']);
  grunt.registerTask('laravel', ['php:laravel']);
  grunt.registerTask('yii', ['php:yii']);
  grunt.registerTask('martini', ['shell:martini']);
  grunt.registerTask('express', ['shell:express']);
  grunt.registerTask('rails', ['shell:rails']);
  grunt.registerTask('django', ['shell:django']);
  grunt.registerTask('nickel', ['shell:nickel']);
  grunt.registerTask('paper', ['watch:paper']);
};
