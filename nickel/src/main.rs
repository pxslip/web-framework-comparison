extern crate nickel;
extern crate nickel_sqlite;

use std::io::net::ip::Ipv4Addr;
use nickel::{ Nickel, Request, Response, HttpRouter };
use nickel_sqlite::Sqlite3Middleware;
use std::collections::HashMap;
use std::macros::fail;

fn main() {
    let mut server = Nickel::new();
    server.utilize(Sqlite3Middleware::new("db-nickel.db".to_string()));

    fn hello_handler (request: &Request, response: &mut Response) {
        let ref mut db = req.db_conn().lock();
        let mut stmt = match db.prepare("select * from hellos;") {
            Ok(s) => s,
            Err(e) => response.send("failed..: {}", e),
        };
        let mut res = stmt.execute();
        let mut hellos = Vec::new();
        match res.step() {
            Some(Ok(ref mut row)) => {
                hellos.push(format!("<h2>{:s}</h2>", row.column_int(1)));
            },
            _ => break,
        };
        let mut data = HashMap::<&str, Vec<Str>>::new();
        data.insert("hellos", hellos);
        response.render("src/tpls/index.tpl", &data);
    }

    server.get("/hello", hello_handler);
    server.listen(Ipv4Addr(127, 0, 0, 1), 6767);
}
