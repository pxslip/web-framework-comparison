package main

import (
  "database/sql"
  "bytes"
  "github.com/go-martini/martini"
  _ "github.com/mattn/go-sqlite3"
)

func main() {
  m := martini.Classic()
  db, _ := sql.Open("sqlite3", "/Users/wskruse/projects/csci6221-term-paper-2014/martini/db-martini.sqlite")
  m.Get("/", func() string {
    //Access the database
    var buffer bytes.Buffer
    rows, _ := db.Query("SELECT text FROM hellos")
    for rows.Next() {
        var text string
        rows.Scan(&text)
        buffer.WriteString(text)
        buffer.WriteString("\n")
    }
    db.Close()
    return buffer.String()
  })
  m.Run()
}

func checkErr(err error, msg string) {
    if err != nil {
        println(err)
        println(msg)
    }
}
