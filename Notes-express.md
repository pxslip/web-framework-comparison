# Notes on ExpressJS #

## Requirements ##
- Node installed http://nodejs.org/

## Installation ##
- Easy way:
  - `npm install -g express-generator`
  - `express <project-name>`
- Alternate without starter files
  - `npm init` - creates package.json
  - `npm install express --save` - installs express and saves it as dependency
