var page = require('webpage'),
  system = require('system'),
  fs = require('fs'),
  t, address, count, delta, filename,
  avg = 0
  loaddata = {avg: 0, data: []},
  i = 1,
  usage = 'Usage: loadspeed.js URL [name] [count] #count defaults to 1000, name defaults to current unix timestamp';

if (system.args.length === 1) {
  console.log(usage);
  phantom.exit();
}
//ternary op to either get param, or use default
count = (system.args[3]) ? parseInt(system.args[3]) : 100;
name = (system.args[2]) ? system.args[2] : Date.now();
if(typeof count !== "number") {
    console.log(usage+"\nCount must be a number, provided: "+count);
    phantom.exit();
}
if(count > 100) {
    console.log("Due to system limitations count must be 100 or less");
    phantom.exit();
}
filename = './loaddata/loaddata-'+name+'.json';
t = Date.now();
address = system.args[1];
console.log("Loading "+address+" "+count+" times");
loadpage(page, i);

function loadpage(lpage, i) {
    lpage.create().open(address, function(status) {
        console.log("Attempt: "+i);
        if (status !== 'success') {
            console.log('FAIL to load the address');
        } else {
            delta = Date.now() - t;
            console.log('Loading time ' + delta + ' msec');
            loaddata.data.push(delta);
            t = Date.now();
        }
        if(i < count) {
            loadpage(lpage, ++i);
        } else if(i === count) {
            for(i = 0; i < loaddata.data.length; i++) {
                avg += loaddata.data[i];
            }
            avg = avg / loaddata.data.length;
            loaddata.avg = avg;
            fs.write(filename, JSON.stringify(loaddata), 'w');
            phantom.exit();
        }
    });
}
