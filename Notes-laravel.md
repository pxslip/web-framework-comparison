# Notes on installing, configuring and running laravel #

- Installation is relatively straightforward
  - Install laravel via composer using `composer global require "laravel/installer=~1.1"`
  - Create a laravel project via `laravel new <project-name>`

- Difficulty with adding laravel executable to path
- Error using the laravel executable, fall back to using composer installer
  - `composer create-project laravel/laravel your-project-name --prefer-dist`
- This method threw error during installation regarding mcrypt extension (possibly?)
- manually installing mcrypt using brew
- Success!

- Default config - must swap from closure based routing to controller based routing
