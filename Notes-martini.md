# Notes on martini #

## Requirements ##

## Installation ##

- `go get github.com/go-martini/martini` will install go-martini to your GOPATH
- reference with `import github.com/go-martini/martini`

## Thoughts ##

Extremely unopinionated, provides methods for routing to function.
Does not include ORM/DB Mapping, can use GORP or go's db drivers
